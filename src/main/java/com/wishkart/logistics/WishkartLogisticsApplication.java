package com.wishkart.logistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class WishkartLogisticsApplication {

	public static void main(String[] args) {
		SpringApplication.run(WishkartLogisticsApplication.class, args);
	}
}
