package com.wishkart.logistics.service;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;

import com.wishkart.logistics.model.OutboundShipping;
import com.wishkart.logistics.model.external.OrderItem;
import com.wishkart.logistics.repository.OutboundShippingRepository;
import com.wishkart.logistics.rest.client.SalesProxy;

public class DispatchServiceImpl implements DispatchService {

	@Autowired
	private OutboundShippingRepository outboundRepo;
	
	@Autowired
	private SalesProxy salesProxy;
	
	@Override
	public void updateDispatchStatus(OutboundShipping outboundShipping) {
		outboundRepo.findById(outboundShipping.getId()).ifPresent(o ->{
			o.setDeliveryStatus(outboundShipping.getDeliveryStatus());
			if(o.getDeliveryStatus().equalsIgnoreCase("DELIVERED")) {
				o.setDeliveredOn(new Timestamp(System.currentTimeMillis()));
				o.setDeliveryComment(outboundShipping.getDeliveryComment());
				outboundRepo.save(o);
				updateOrder(o);
			}
		});

	}

	private void updateOrder(OutboundShipping o) {
		OrderItem orderItem = OrderItem.builder()
				.id(o.getItemId())
				.deliveryComment(o.getDeliveryComment())
				.deliveredOn(o.getDeliveredOn()).build();
		salesProxy.updateDeliveryStatus(orderItem);
	}

}
