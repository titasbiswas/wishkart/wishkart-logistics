package com.wishkart.logistics.config;

import org.springframework.cloud.stream.annotation.EnableBinding;

import com.wishkart.logistics.messaging.ShippingStreams;



@EnableBinding(ShippingStreams.class)
public class StreamsConfig {

}
