package com.wishkart.logistics.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wishkart.logistics.model.OutboundShipping;

public interface OutboundShippingRepository extends JpaRepository<OutboundShipping, Long> {

}
