package com.wishkart.logistics.model.external;

import java.io.Serializable;
import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the order_details database table.
 * 
 */
@Entity
@Table(name="order_details")
@NamedQuery(name="OrderDetail.findAll", query="SELECT o FROM OrderDetail o")
@Data
@Builder
@AllArgsConstructor
public class OrderDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@Column(name="bill_addr_id")
	private Long billAddrId;

	@Column(name="invoice_no")
	private String invoiceNo;

	@Column(name="order_date")
	private Timestamp orderDate;

	@Column(name="ship_addr_id")
	private Long shipAddrId;

	@Column(name="total_amount")
	private BigDecimal totalAmount;

	//bi-directional many-to-one association to OrderItem
	@OneToMany(mappedBy="orderDetail", fetch=FetchType.EAGER, cascade = CascadeType.ALL)
	private List<OrderItem> orderItems;

	public OrderDetail() {
	}



	public OrderItem addOrderItem(OrderItem orderItem) {
		getOrderItems().add(orderItem);
		orderItem.setOrderDetail(this);

		return orderItem;
	}

	public OrderItem removeOrderItem(OrderItem orderItem) {
		getOrderItems().remove(orderItem);
		orderItem.setOrderDetail(null);

		return orderItem;
	}

}