package com.wishkart.logistics.model.external;

import java.io.Serializable;
import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;


/**
 * The persistent class for the order_items database table.
 * 
 */

@Data
@Builder
@AllArgsConstructor
public class OrderItem implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;

	private BigDecimal amount;


	private Timestamp deliveredOn;


	private String deliveryComment;

	private BigDecimal discountPrcntg;


	private String productId;

	private Integer qty;

	private BigDecimal rate;

	private OrderDetail orderDetail;

	public OrderItem() {
	}

	

}