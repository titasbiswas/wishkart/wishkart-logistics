package com.wishkart.logistics.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.wishkart.logistics.model.OutboundShipping;
import com.wishkart.logistics.repository.OutboundShippingRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ShippingListner {
	
	@Autowired
	private OutboundShippingRepository repo;
	
	@StreamListener(ShippingStreams.INPUT)
	public void recieveOutboundShipping(@Payload OutboundShipping outboundShipping) {
		log.info("recieved outbound shipping ", outboundShipping);
		//TODO - call service to insert shipping to db
		outboundShipping.setDeliveryStatus("PREPARING_DISPATCH");
		repo.save(outboundShipping);
	}
	
}
